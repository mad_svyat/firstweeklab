package com.madsvyat.firstweeklab;

import com.madsvyat.firstweeklab.model.FileTextResource;
import com.madsvyat.firstweeklab.model.TextResource;
import com.madsvyat.firstweeklab.model.UrlTextResource;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class Main {

    private static final String FILE_KEY = "-f";
    private static final String URL_KEY = "-u";

    /**
     * @param args default launch arguments for files in repository is
     *             <code>-f test.txt -f test2.txt -u http://skipy.ru/technics/objCompTh.html<code/>
     */
    public static void main(String[] args) {

        List<TextResource> resources = new ArrayList<>();

        if (args.length == 0 || args.length % 2 != 0) {
            System.out.println("Incorrect arguments, should be set of " +
                    "[-f <path>] and [-u <url>]\nusing default arguments");
            resources.add(new FileTextResource("test.txt"));
            resources.add(new FileTextResource("test2.txt"));
            resources.add(new UrlTextResource("http://skipy.ru/technics/objCompTh.html"));
        } else {
            for (int i = 0; i < args.length; i += 2) {
                if (FILE_KEY.equals(args[i])) {
                    resources.add(new FileTextResource(args[i + 1]));
                } else if(URL_KEY.equals(args[i])) {
                    resources.add(new UrlTextResource(args[i + 1]));
                } else {
                    System.out.println("Unknown argument key '" + args[i] + "'");
                }
            }
        }

        Engine engine = new Engine(resources);
        engine.start();
    }

}
