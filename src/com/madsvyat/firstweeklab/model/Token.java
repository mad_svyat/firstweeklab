package com.madsvyat.firstweeklab.model;

/**
 *
 */
public class Token {

    public enum Type {WORD, CYRILLIC_WORD, UNKNOWN};

    private final Type type;
    private final String value;

    public Token(Type type, String value) {
        this.type = type;
        this.value = value;
    }

    @Override
    public int hashCode() {
        return 31 * type.hashCode() + value.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof Token)) {
            return false;
        }

        if (!type.equals(((Token) obj).type) && !value.equals(((Token) obj).value)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return value;
    }
}
