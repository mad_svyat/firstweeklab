package com.madsvyat.firstweeklab.model;

import java.util.Map;

/**
 *
 */
public interface ResultsChangedListener {

    void onResultsChanged(Map<Token, Integer> results);

    void onCalculationFinished(Map<Token, Integer> results);
}
