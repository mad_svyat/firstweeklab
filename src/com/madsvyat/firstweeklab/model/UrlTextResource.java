package com.madsvyat.firstweeklab.model;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 *
 */
public class UrlTextResource extends TextResource {

    private String url;

    public UrlTextResource(String url) {
        this.url = url;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        URLConnection connection = new URL(url).openConnection();
        return connection.getInputStream();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }

        if (!(o instanceof TextResource)) {
            return false;
        }

        UrlTextResource another = (UrlTextResource) o;
        if ((url != null && !url.equals(another.url))
                || (url == null && another.url != null)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return url != null ? url.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "UrlTextResource{" +
                "url='" + url + '\'' +
                '}';
    }
}
