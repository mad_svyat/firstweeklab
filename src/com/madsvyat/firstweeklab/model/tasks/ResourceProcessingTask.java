package com.madsvyat.firstweeklab.model.tasks;

import com.madsvyat.firstweeklab.model.CyrillicWordsTokenizer;
import com.madsvyat.firstweeklab.model.TextResource;
import com.madsvyat.firstweeklab.model.Token;
import com.madsvyat.firstweeklab.model.Tokenizer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.BlockingQueue;

/**
 *
 */
public class ResourceProcessingTask implements Runnable, Tokenizer.OnTokenReceivedListener {

    private TextResource resource;
    private Tokenizer tokenizer;
    private final BlockingQueue<Token> resultsQueue;

    public ResourceProcessingTask(TextResource resource, BlockingQueue<Token> resultsQueue) {
        this.resource = resource;
        this.resultsQueue = resultsQueue;
        this.tokenizer = new CyrillicWordsTokenizer();
        tokenizer.registerOnTokenReceivedListener(this);
    }

    @Override
    public void run() {
        try (InputStream is = resource.getInputStream();
             BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
            int ch;
            while ((ch = reader.read()) != -1) {
                tokenizer.putChar((char) ch);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTokenReceived(Token token) {
        try {
            resultsQueue.put(token);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
