package com.madsvyat.firstweeklab.model.tasks;

import com.madsvyat.firstweeklab.model.ResultsChangedListener;
import com.madsvyat.firstweeklab.model.Token;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 *
 */
public class ResultsAggregatorTask implements Runnable {

    private boolean completed;
    private Map<Token, Integer> resultsMap;
    private final BlockingQueue<Token> resultsQueue;
    private ResultsChangedListener resultsChangedListener;


    public ResultsAggregatorTask(BlockingQueue<Token> resultsQueue,
                                 ResultsChangedListener resultsChangedListener) {
        this.resultsMap = new ConcurrentHashMap<>();
        this.resultsQueue = resultsQueue;
        this.resultsChangedListener = resultsChangedListener;
    }

    @Override
    public void run() {

        while (!completed) {
            try {
                Token token = resultsQueue.poll(5, TimeUnit.SECONDS);
                if (token != null) {
                    Integer count = resultsMap.get(token);

                    if (count == null) {
                        resultsMap.put(token, 1);
                    } else {
                        resultsMap.put(token, ++count);
                    }
                    resultsChangedListener.onResultsChanged(resultsMap);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        resultsChangedListener.onCalculationFinished(resultsMap);
    }

    public void awaitComplete() {
        while (true) {

            if (resultsQueue.isEmpty()) {
                completed = true;
                return;
            }
        }
    }
}
