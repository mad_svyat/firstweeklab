package com.madsvyat.firstweeklab.model;

import java.io.IOException;
import java.io.InputStream;

/**
 *
 */
public abstract class TextResource {

    public abstract InputStream getInputStream() throws IOException;
}
