package com.madsvyat.firstweeklab.model;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 */
public class FileTextResource extends TextResource {

    private String path;

    public FileTextResource(String path) {
        this.path = path;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return new FileInputStream(path);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }

        if (!(o instanceof TextResource)) {
            return false;
        }

        FileTextResource another = (FileTextResource) o;
        if ((path != null && !path.equals(another.path))
                || (path == null && another.path != null)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return path != null ? path.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "FileTextResource{" +
                "path='" + path + '\'' +
                '}';
    }
}
