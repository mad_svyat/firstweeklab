package com.madsvyat.firstweeklab.model;

/**
 *
 */
public interface Tokenizer {

    void putChar(char ch);

    void registerOnTokenReceivedListener(OnTokenReceivedListener listener);

    interface OnTokenReceivedListener {
        void onTokenReceived(Token token);
    }
}
