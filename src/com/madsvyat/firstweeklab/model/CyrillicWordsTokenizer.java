package com.madsvyat.firstweeklab.model;

import java.util.regex.Pattern;

/**
 *
 */
public class CyrillicWordsTokenizer implements Tokenizer {

    private static final Pattern CYRILLIC_WORD_PATTERN = Pattern.compile("[а-яёА-ЯЁ]+");

    private OnTokenReceivedListener tokenReceivedListener;
    private StringBuilder currentTokenValue;
    private Token.Type currentTokenType;
    private boolean tokenStarted;

    @Override
    public void putChar(char ch) {
        if (tokenStarted) {

            if (currentTokenType == Token.Type.WORD) {
                if (Character.isLetter(ch)) {
                    currentTokenValue.append(ch);
                } else {
                    finishToken();
                    startNewToken(ch);
                }
            } else {
                if (Character.isLetter(ch)) {
                    finishToken();
                    startNewToken(ch);
                }
            }
        } else {
            startNewToken(ch);
        }
    }

    @Override
    public void registerOnTokenReceivedListener(OnTokenReceivedListener listener) {
        this.tokenReceivedListener = listener;
    }

    private void startNewToken(char ch) {
        tokenStarted = true;
        currentTokenValue = new StringBuilder();

        if (Character.isAlphabetic(ch)) {
            currentTokenType = Token.Type.WORD;
            currentTokenValue.append(ch);
        }  else {
            currentTokenType = Token.Type.UNKNOWN;
        }
    }

    private void finishToken() {
        tokenStarted = false;
        if (currentTokenType == Token.Type.WORD) {
            String word = currentTokenValue.toString();

            if (CYRILLIC_WORD_PATTERN.matcher(word).matches()) {
                Token cyrillicWord = new Token(Token.Type.CYRILLIC_WORD, word);

                if (tokenReceivedListener != null) {
                    tokenReceivedListener.onTokenReceived(cyrillicWord);
                }
            }
        }
    }
}
