package com.madsvyat.firstweeklab.view;

import com.madsvyat.firstweeklab.model.Token;

import java.util.Map;

/**
 *
 */
public class ConsoleViewer implements ResultsViewer {

    private int counter;

    @Override
    public void displayResults(Map<Token, Integer> results) {
        System.out.println("Iteration: " + (++counter));
        results.forEach((key, value) -> System.out.println(key + " : " + value));
        System.out.println();
    }

    @Override
    public void displayFinalResults(Map<Token, Integer> results) {
        System.out.println("\n\nCalculation finished:");
        results.forEach((key, value) -> System.out.println(key + " : " + value));
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Completed. Total unique words: " + results.size());

    }
}
