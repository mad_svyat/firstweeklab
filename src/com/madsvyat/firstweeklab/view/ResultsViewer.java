package com.madsvyat.firstweeklab.view;

import com.madsvyat.firstweeklab.model.Token;

import java.util.Map;

/**
 *
 */
public interface ResultsViewer {

    void displayResults(Map<Token, Integer> results);

    void displayFinalResults(Map<Token, Integer> results);

}
