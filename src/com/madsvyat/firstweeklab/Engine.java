package com.madsvyat.firstweeklab;

import com.madsvyat.firstweeklab.model.ResultsChangedListener;
import com.madsvyat.firstweeklab.model.TextResource;
import com.madsvyat.firstweeklab.model.Token;
import com.madsvyat.firstweeklab.model.tasks.ResourceProcessingTask;
import com.madsvyat.firstweeklab.model.tasks.ResultsAggregatorTask;
import com.madsvyat.firstweeklab.view.ConsoleViewer;
import com.madsvyat.firstweeklab.view.ResultsViewer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;

/**
 *
 */
public class Engine implements ResultsChangedListener {

    private List<TextResource> resources;
    private BlockingQueue<Token> resultsQueue;
    private ExecutorService workingPool;
    private ResultsViewer resultsViewer;

    public Engine(List<TextResource> resources) {
        this.resources = resources;
        this.resultsQueue = new LinkedBlockingQueue<>();
        this.workingPool = Executors.newFixedThreadPool(resources.size());
        this.resultsViewer = new ConsoleViewer();
    }

    public void start() {
        ResultsAggregatorTask aggregatorTask = new ResultsAggregatorTask(resultsQueue, this);
        Thread aggregatorThread = new Thread(aggregatorTask);
        aggregatorThread.start();

        List<Future<?>> futures = new ArrayList<>();
        for (TextResource resource : resources) {
            Future<?> future = workingPool
                    .submit(new ResourceProcessingTask(resource, resultsQueue));
            futures.add(future);
        }

        for (Future<?> future : futures) {
            try {
                future.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        workingPool.shutdown();
        aggregatorTask.awaitComplete();
    }

    @Override
    public void onResultsChanged(Map<Token, Integer> results) {
        resultsViewer.displayResults(results);

    }

    @Override
    public void onCalculationFinished(Map<Token, Integer> results) {
        resultsViewer.displayFinalResults(results);
    }
}
